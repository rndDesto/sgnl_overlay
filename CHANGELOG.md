

## 0.0.41 (2023-09-27)


### Bug Fixes

* update overlay ([a886b3a](https://gitlab.com/rndDesto/sgnl_overlay/commit/a886b3a77d8fe9ca1317533f53e0aff5a63dc092))

## 0.0.40 (2023-09-21)


### Bug Fixes

* update import global css ([9f1e68b](https://gitlab.com/rndDesto/sgnl_overlay/commit/9f1e68b59e20f26e713df7692f696d51d2ceafb2))

## 0.0.39 (2023-09-20)


### Bug Fixes

* renew token ([b24165d](https://gitlab.com/rndDesto/sgnl_overlay/commit/b24165d0d4bad3e17c17317d2bf5fce738e49c9d))

## 0.0.38 (2023-09-20)


### Bug Fixes

* edit css minor ([6e04ccd](https://gitlab.com/rndDesto/sgnl_overlay/commit/6e04ccd6fd4c537f2988c4191d7bdfec4e171f73))
* ganti targer rilis registry k repo overlay ([b9d0a81](https://gitlab.com/rndDesto/sgnl_overlay/commit/b9d0a8125d7c1a1de25f1c8553a306c05db0ba22))
* new paket ([87804ce](https://gitlab.com/rndDesto/sgnl_overlay/commit/87804ce89309bd6dbda787efcfcba57727526dd0))
* push tanpa rilis ([55a38ec](https://gitlab.com/rndDesto/sgnl_overlay/commit/55a38ec00eda00f8c0394b11057cfe91e45b222b))
* renew token ([fab78c4](https://gitlab.com/rndDesto/sgnl_overlay/commit/fab78c402c3ce21014390f6db2d884fa76f0700d))
* rm bump releiase ([858a2e2](https://gitlab.com/rndDesto/sgnl_overlay/commit/858a2e212fc6b2b157f81b936417542fa8b0616e))
* test rilis lagi ([c01da63](https://gitlab.com/rndDesto/sgnl_overlay/commit/c01da63ac9692a22f129cd73f5d048bf431904ab))
* update snackbar ([2985c47](https://gitlab.com/rndDesto/sgnl_overlay/commit/2985c47a7e4e9ea56d4ae6cdf1fe1ef1ba42373f))
* update snackbar ([8923309](https://gitlab.com/rndDesto/sgnl_overlay/commit/8923309143eb8ca43936ba9229f4923cbfede3bd))
* update snackbar ([936b2eb](https://gitlab.com/rndDesto/sgnl_overlay/commit/936b2eb00d80311f1904eb2800d9d0ff7130eb1a))

## 0.0.37 (2023-09-20)


### Bug Fixes

* edit css minor ([6e04ccd](https://gitlab.com/rndDesto/sgnl_overlay/commit/6e04ccd6fd4c537f2988c4191d7bdfec4e171f73))
* ganti targer rilis registry k repo overlay ([b9d0a81](https://gitlab.com/rndDesto/sgnl_overlay/commit/b9d0a8125d7c1a1de25f1c8553a306c05db0ba22))
* new paket ([87804ce](https://gitlab.com/rndDesto/sgnl_overlay/commit/87804ce89309bd6dbda787efcfcba57727526dd0))
* renew token ([fab78c4](https://gitlab.com/rndDesto/sgnl_overlay/commit/fab78c402c3ce21014390f6db2d884fa76f0700d))
* rm bump releiase ([858a2e2](https://gitlab.com/rndDesto/sgnl_overlay/commit/858a2e212fc6b2b157f81b936417542fa8b0616e))
* test rilis lagi ([c01da63](https://gitlab.com/rndDesto/sgnl_overlay/commit/c01da63ac9692a22f129cd73f5d048bf431904ab))
* update snackbar ([2985c47](https://gitlab.com/rndDesto/sgnl_overlay/commit/2985c47a7e4e9ea56d4ae6cdf1fe1ef1ba42373f))
* update snackbar ([8923309](https://gitlab.com/rndDesto/sgnl_overlay/commit/8923309143eb8ca43936ba9229f4923cbfede3bd))
* update snackbar ([936b2eb](https://gitlab.com/rndDesto/sgnl_overlay/commit/936b2eb00d80311f1904eb2800d9d0ff7130eb1a))

## 0.0.29 (2023-09-19)


### Bug Fixes

* frm svg snackbar ([7ae8eb1](https://gitlab.com/rndDesto/sgnl_overlay/commit/7ae8eb14cadf84c59fb18398a9497296cb80671b))
* refactor snackbar ([1f26dd6](https://gitlab.com/rndDesto/sgnl_overlay/commit/1f26dd62cdc17c7a923698020ed41fea43dfbac2))

## 0.0.28 (2023-09-19)


### Bug Fixes

* add callout ([24e6d7f](https://gitlab.com/rndDesto/sgnl_overlay/commit/24e6d7fd42c2eac703157115077c308f684fe680))
* fixing css ([b45a4b6](https://gitlab.com/rndDesto/sgnl_overlay/commit/b45a4b65ac22506cfed5b28149ccd7933289cd1c))
* modal head n foot ([2d3ddd8](https://gitlab.com/rndDesto/sgnl_overlay/commit/2d3ddd8cb8dd1f1b8f8123df3228e8981ef85188))

## 0.0.27 (2023-09-04)


### Bug Fixes

* fixing css ([b45a4b6](https://gitlab.com/rndDesto/sgnl_overlay/commit/b45a4b65ac22506cfed5b28149ccd7933289cd1c))

## 0.0.26 (2023-09-04)


### Bug Fixes

* modal head n foot ([2d3ddd8](https://gitlab.com/rndDesto/sgnl_overlay/commit/2d3ddd8cb8dd1f1b8f8123df3228e8981ef85188))

## 0.0.25 (2023-09-04)


### Bug Fixes

* update weekly ([c35558c](https://gitlab.com/rndDesto/sgnl_overlay/commit/c35558c24a470aad3348fb293205f7b5f738eb2a))

## 0.0.24 (2023-08-28)


### Bug Fixes

* delete local style ([f3a9c44](https://gitlab.com/rndDesto/sgnl_overlay/commit/f3a9c44f9783b6e411b07313a3e21002f2ed77e6))

## 0.0.23 (2023-08-28)


### Bug Fixes

* external style ([29c7826](https://gitlab.com/rndDesto/sgnl_overlay/commit/29c78268e01564079490af1145e6cffa34136318))

## 0.0.22 (2023-08-24)


### Bug Fixes

* center modal ([63f3435](https://gitlab.com/rndDesto/sgnl_overlay/commit/63f34350bb461662ba9bfccb611f257afa70ca5a))

## 0.0.21 (2023-08-22)


### Bug Fixes

* beta grid ([5f2ca4c](https://gitlab.com/rndDesto/sgnl_overlay/commit/5f2ca4c8e1dc38df0b94cba184862b181cda7208))
* ganti nama paket ([d6a6890](https://gitlab.com/rndDesto/sgnl_overlay/commit/d6a689070e669d6d59fd9c6aae9b966bc2f62542))

## 0.0.20 (2023-08-21)


### Bug Fixes

* tambah handler close ([de48ab3](https://gitlab.com/rndDesto/sgnl_overlay/commit/de48ab364673ad64b9a6d37cf17e84e816f95615))

## 0.0.19 (2023-08-21)


### Bug Fixes

* modal inti ([d4417bd](https://gitlab.com/rndDesto/sgnl_overlay/commit/d4417bd9b5605c92f62274e8373ade52afc8f78f))

## 0.0.18 (2023-08-21)


### Bug Fixes

* init commit overlay ([b54c16d](https://gitlab.com/rndDesto/sgnl_overlay/commit/b54c16d419e7cfd7903ef17c20b9cd0ffab3b157))

## 0.0.17 (2023-08-21)


### Bug Fixes

* publish ke sgnl https://gitlab.com/rndDesto/sgnl/-/packages ([2d585d2](https://gitlab.com/rndDesto/sgnl_typography/commit/2d585d2ddcc5a7ca02d0e54770eee241701c559c))

## 0.0.16 (2023-08-18)


### Bug Fixes

* enhance folder ([c4a92dd](https://gitlab.com/rndDesto/sgnl_typography/commit/c4a92dd9ed70dfbb0bea69ae2aa36f22185a79fb))

## 0.0.15 (2023-08-14)


### Bug Fixes

* color title ([2672ae0](https://gitlab.com/rndDesto/sgnl_typography/commit/2672ae033ed6941762562add213974c65e78ce2a))

## 0.0.14 (2023-08-14)


### Bug Fixes

* coba publish lagi ([c64db46](https://gitlab.com/rndDesto/sgnl_typography/commit/c64db460dca6f6f658ba4784fcb40ea3b30ccfef))

## 0.0.13 (2023-08-14)


### Bug Fixes

* tambah script before ([6b63519](https://gitlab.com/rndDesto/sgnl_typography/commit/6b6351918000e56ed25b4efd7bb2cd7358a5317e))

## 0.0.12 (2023-08-14)


### Bug Fixes

* fixing body ([400258d](https://gitlab.com/rndDesto/sgnl_typography/commit/400258d6e6f167b44dbcf49b01976d251508c21d))

## 0.0.11 (2023-08-14)


### Bug Fixes

* edit signal-body ([cc34066](https://gitlab.com/rndDesto/sgnl_typography/commit/cc340660a0050b6f01357a496f1a7fb499e7b082))

## 0.0.10 (2023-08-14)


### Bug Fixes

* cb css ([facbdcd](https://gitlab.com/rndDesto/sgnl_typography/commit/facbdcd17089aa9651e496f3c78cea1720c1fd7e))

## 0.0.9 (2023-08-14)


### Bug Fixes

* coba css dari luar ([6f87936](https://gitlab.com/rndDesto/sgnl_typography/commit/6f879367e7ec57b83576414a1adde70a606d2097))
* coba css dari luar ([691bfae](https://gitlab.com/rndDesto/sgnl_typography/commit/691bfae212ce0fa1bca0155923ca493e032d1d92))
* **coba css dari luar:** coba apply css dari existing class ([303e21b](https://gitlab.com/rndDesto/sgnl_typography/commit/303e21b7378393f4f98acf7fa89584fcb66f9c51))
* coba style dari luar ([06313e8](https://gitlab.com/rndDesto/sgnl_typography/commit/06313e88cfc56b8f2b9d107a2586b25c60300955))

## 0.0.8 (2023-08-11)


### Bug Fixes

* coba republish ([9718f10](https://gitlab.com/rndDesto/sgnl_typography/commit/9718f10d6a8b1191ac998d658f106a95d2fed384))
* publihs lagi ([8c8edb9](https://gitlab.com/rndDesto/sgnl_typography/commit/8c8edb9b057536f60e9a2ca2a33fedf2ae1a980c))

## 0.0.7 (2023-08-11)


### Bug Fixes

* apus config paket ([1c21e46](https://gitlab.com/rndDesto/sgnl_typography/commit/1c21e462efd6cc6a6cf9c5029d29637c7eac0bc1))

## 0.0.6 (2023-08-11)


### Bug Fixes

* rebuild lagi ([5413279](https://gitlab.com/rndDesto/sgnl_typography/commit/5413279860e43ea119c3025ea7eba7a47abdc568))

## 0.0.5 (2023-08-11)


### Bug Fixes

* tambah heading ([699e4ef](https://gitlab.com/rndDesto/sgnl_typography/commit/699e4ef6cf875e474df29af30dbd639e15db3445))

## 0.0.4 (2023-08-10)


### Bug Fixes

* add native style ([000edc2](https://gitlab.com/rndDesto/sgnl_typography/commit/000edc2548dc153389a362a1dde692ef3ace3665))

## 0.0.3 (2023-08-10)


### Bug Fixes

* better folder ([d6f1800](https://gitlab.com/rndDesto/sgnl_typography/commit/d6f1800d3f0971b8cb7eb5bf5cbdfc6318868cc1))

## 0.0.2 (2023-08-10)


### Bug Fixes

* default bundle ([0a4eed4](https://gitlab.com/rndDesto/sgnl_typography/commit/0a4eed45e7a5332d44154e90571ebcc99b6203c1))
* fixing publish ([345e8e0](https://gitlab.com/rndDesto/sgnl_typography/commit/345e8e07062c98a41a7aabaa9a2e79f88cdb4115))

## 0.0.1 (2023-08-10)


### Bug Fixes

* **init commit:** inisial commit ([670cac2](https://gitlab.com/rndDesto/sgnl_typography/commit/670cac2a4f6376bd4fa9c6ac78ced2e33b6aeafc))