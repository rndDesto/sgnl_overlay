import { css, html, LitElement, PropertyValues, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import styles from './snackbar.scss?inline';
import { when } from 'lit/directives/when.js';

@customElement('signal-snackbar')
export class SignalSnackBar extends LitElement {

    static styles = [
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    @property({ type: Boolean })
    isOpen=false;

    @property({ type: String })
    color?: 'primary' | 'secondary' | 'tritary' | 'valid'  | 'info' | 'warning' | 'error' | 'disable' = 'primary';

    @property({ type: String })
    position?: 'top' | 'bottom' = 'top';

    @property({ type: Number })
    duration? = 0;

    private timeoutId?: any;


    onClose(){
        this.isOpen = false;
        this.dispatchEvent(new CustomEvent('onClose', { detail: { isOpen: this.isOpen } }));

        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
        }
        
    }

    startTimeout() {
        if (this.duration) {
            this.timeoutId = setTimeout(() => {
                if (this.isOpen) {
                    this.onClose();
                }
            }, this.duration);
        }
    }

    updated(changedProperties: PropertyValues) {
        super.updated(changedProperties);

        if (changedProperties.has('isOpen') && this.isOpen) {
            this.startTimeout();
        }
    }

    renderContent(){
        const colorProps = this.color ?`box-${this.color}`:'';
        const positionProps = this.position ?`pos-${this.position}`:'';
        const styledClasses = classMap({
            "tsel-snackbar-wrapper":true,
            [positionProps]: !!this.position,
        });

        const variatClasses = classMap({
            "tsel-snackbar-main":true,
            [colorProps]: !!this.color,
        });
       
        return html`
        <div 
            data-testid=${this.testId === '' ?'data-test-calloutid': this.testId}
            part=${this.partName === '' ?'tselpartbody': this.partName}
            id=${this.idName === '' ?'tsel-id-callout': this.idName} 
            class=${styledClasses}
            >
                <div class=${variatClasses}>
                    <div class="snackbar-konten">
                        <slot></slot>
                    </div>
                    <div class="snackbar-action" @click=${this.onClose}>
                        <slot name="action"></slot>
                    </div>
                </div>
        </div>
        `
    }

    render() {
        return when(this.isOpen, 
            ()=> this.renderContent(), 
            ()=> '')
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-snackbar': SignalSnackBar
    }
}