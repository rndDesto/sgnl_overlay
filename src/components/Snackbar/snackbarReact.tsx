import React from "react";
import { SignalSnackBar } from "./snackbar";
import { createComponent } from "@lit-labs/react";

const SignalSnackBarReact = createComponent({
    tagName: 'signal-snackbar',
    elementClass: SignalSnackBar,
    react: React,
    events: {
        onClick: 'onClick',
        onClose: 'onClose',
    },
});

export default SignalSnackBarReact