import { css, html, LitElement, svg, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import styles from './callout.scss?inline';
import { when } from 'lit/directives/when.js';

@customElement('signal-callout')
export class SignalCallout extends LitElement {

    static styles = [
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    @property({ type: Boolean })
    isOpen=false;

    @property({ type: Boolean })
    isFixed?=false;

    @property({ type: Boolean })
    withClose?=false;

    @property({ type: Boolean })
    withIcon?=false;

    @property({ type: String })
    color?: 'primary' | 'secondary' | 'tritary' | 'valid'  | 'info' | 'warning' | 'error' | 'disable' = 'primary';


    onClose(){
        this.isOpen = false
        this.dispatchEvent(new CustomEvent('onClose', { detail: { isOpen: this.isOpen } }));
    }

    svgClose(){
        return svg`
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M19.0795 3.3295C19.5188 2.89017 20.2312 2.89017 20.6705 3.3295C21.1098 3.76884 21.1098 4.48116 20.6705 4.9205L13.591 12L20.6705 19.0795C21.1098 19.5188 21.1098 20.2312 20.6705 20.6705C20.2312 21.1098 19.5188 21.1098 19.0795 20.6705L12 13.591L4.9205 20.6705C4.48116 21.1098 3.76884 21.1098 3.32951 20.6705C2.89016 20.2312 2.89016 19.5188 3.32951 19.0795L10.409 12L3.32951 4.9205C2.89017 4.48116 2.89017 3.76884 3.32951 3.3295C3.76885 2.89017 4.48116 2.89017 4.9205 3.3295L12 10.409L19.0795 3.3295Z" fill="#B3BAC6"/>
        </svg>
        `
    }

    svgInfo(){
        return svg`
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 4.22222C7.70445 4.22222 4.22222 7.70445 4.22222 12C4.22222 16.2955 7.70445 19.7778 12 19.7778C16.2955 19.7778 19.7778 16.2955 19.7778 12C19.7778 7.70445 16.2955 4.22222 12 4.22222ZM2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12Z" fill="#0050AE"/>
            <path d="M13 8C13 8.55228 12.5523 9 12 9C11.4477 9 11 8.55228 11 8C11 7.44772 11.4477 7 12 7C12.5523 7 13 7.44772 13 8Z" fill="#0050AE"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M10 11C10 10.4477 10.4477 10 11 10H12C12.5523 10 13 10.4477 13 11L13 15C13.5523 15 14 15.4477 14 16C14 16.5523 13.5523 17 13 17H12C11.4477 17 11 16.5523 11 16V12C10.4477 12 10 11.5523 10 11Z" fill="#0050AE"/>
        </svg>
    `
    }

    svgWarning(){
        return svg`
        <svg width="24" height="24" viewBox="0 0 24 24" fill="#FDA22B" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M12 5.60509L5.276 18.9897H18.724L12 5.60509ZM10.693 3.81723C11.2404 2.72759 12.7596 2.72759 13.307 3.81723L20.8348 18.8019C21.3389 19.8053 20.6285 21 19.5278 21H4.47223C3.3715 21 2.66111 19.8053 3.1652 18.8019L10.693 3.81723Z" fill="#FDA22B"/>
            <path d="M13 17C13 17.5523 12.5523 18 12 18C11.4477 18 11 17.5523 11 17C11 16.4477 11.4477 16 12 16C12.5523 16 13 16.4477 13 17Z" fill="#FDA22B"/>
            <path d="M11 11C11 10.4477 11.4477 10 12 10C12.5523 10 13 10.4477 13 11V14C13 14.5523 12.5523 15 12 15C11.4477 15 11 14.5523 11 14V11Z" fill="#FDA22B"/>
        </svg>
        `
    }

    renderClose(){
        return when(this.withClose, 
            ()=> html`
            <div class="callout-action-right" @click=${this.onClose}>
                ${this.svgClose()}
            </div>`, 
            ()=> html``)
    }

    renderIcon(){
        const IconType = () => {
            switch (this.color) {
                case 'primary':
                    return this.svgInfo();
                case 'warning':
                    return this.svgWarning();
                default:
                    return '';
            }
        };


        return when(this.withIcon, 
            ()=> html`
            <div class="callout-icon">
                ${IconType()}
            </div>`, 
            ()=> html``)
    }


    renderContent(){
        const colorProps = this.color ?`box-${this.color}`:'';
        const styledClasses = classMap({
            "tsel-callout-wrapper":true,
            "fixed": !!this.isFixed,
        });

        const variatClasses = classMap({
            "tsel-callout-main":true,
            [colorProps]: !!this.color,
        });
       
        return html`
        <div 
            data-testid=${this.testId === '' ?'data-test-calloutid': this.testId}
            part=${this.partName === '' ?'tselpartbody': this.partName}
            id=${this.idName === '' ?'tsel-id-callout': this.idName} 
            class=${styledClasses}
            >
                <div class=${variatClasses}>
                    ${this.renderIcon()}
                    <div class="callout-konten">
                        <slot></slot>
                    </div>
                    ${this.renderClose()}
                </div>
        </div>
        `
    }

    render() {
        return when(this.isOpen, 
            ()=> this.renderContent(), 
            ()=> '')
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-callout': SignalCallout
    }
}