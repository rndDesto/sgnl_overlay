import React from "react";
import { SignalCallout } from "./callout";
import { createComponent } from "@lit-labs/react";

const SignalCalloutReact = createComponent({
    tagName: 'signal-callout',
    elementClass: SignalCallout,
    react: React,
    events: {
        onClick: 'onClick',
        onClose: 'onClose',
    },
});

export default SignalCalloutReact