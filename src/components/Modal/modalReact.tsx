import React from "react";
import { SignalModal } from "./modal";
import { createComponent } from "@lit-labs/react";

const SignalModalReact = createComponent({
    tagName: 'signal-modal',
    elementClass: SignalModal,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalModalReact