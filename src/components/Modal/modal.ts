import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import styles from './modal.scss?inline';
import { when } from 'lit/directives/when.js';

@customElement('signal-modal')
export class SignalModal extends LitElement {
    static styles = [
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';
   
    @property({ type: Boolean })
    isOpen=false;

    @property({ type: Boolean })
    onCloseBackdrop?=false;

    @property({ reflect: true })
    size: 'xl' | 'lg' | 'md' | 'sm' = 'md';

    onClose(){
        this.isOpen = false
        this.dispatchEvent(new CustomEvent('onClose', { detail: { isOpen: this.isOpen } }));
    }

    renderBackDrop(){
        return when(this.onCloseBackdrop, 
            ()=> html`<div class="overlay" @click=${this.onClose}></div>`, 
            ()=> html`<div class="overlay"></div>`)
    }

    renderContent(){
        const sizeClass = this.size ?`size-${this.size}`:'';

        const styledClasses = classMap({
            "tsel-modal-paper":true,
            [sizeClass]: !!this.size,
        });
        return html`
        <div 
            data-testid=${this.testId === '' ?'data-test-modalid': this.testId}
            part=${this.partName === '' ?'tselpartbody': this.partName}
            id=${this.idName === '' ?'tsel-id-modal': this.idName} 
            class="tsel-modal-wrapper"
            >
                ${this.renderBackDrop()}
                <div class="tsel-modal-container">
                    <div class=${styledClasses}>
                        <div class="modal-paper-header">
                            <slot name="header"></slot>
                        </div>
                        <div class="modal-paper-content">
                            <slot></slot>
                        </div>
                        <div class="modal-paper-footer">
                            <slot name="footer"></slot>
                        </div>
                    </div>
                </div>
        </div>
        `
    }

    

    render() {
        return when(this.isOpen, 
            ()=> this.renderContent(), 
            ()=> '')
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-modal': SignalModal
    }
}